﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StageManager : MonoBehaviour
{
    static StageManager instance;
    public static StageManager Instance { get { return instance; } }

    public Sprite[] bgImg; // 뒷배경 이미지
    public Sprite groundImg; // 바닥 이미지
    public int totalSection; // 총 섹션 수

    public Transform startPosition; // 플레이어 시작 지점.
    public GameObject playerPF; // 플레이어 프리팹.
    public static GameObject playerObj; // 플레이어 오브젝트. 

    public List<InvenSlot> rewards = new List<InvenSlot>(); // 플레이어가 획득한 보상 목록.

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        SectionManager.Instance.Setup();
        EnemySpawner.Instance.Setup();
        BGMove.Instance.Setup();

        playerObj = (GameObject)Instantiate(playerPF, startPosition.position, Quaternion.identity);
        UI_inGame.Instance.Setup();
    }

    public void AddReward(int index, int amount) // 보상 목록에 추가. (실제 주는게 아님)
    {
        for (int i = 0; i<rewards.Count; i++)
        {
            if(rewards[i]._itemCode == index)
            {
                rewards[i]._amount += amount; // 이미 보상 리스트에 있는종류의 아이탬. 수량만 추가.
                return;
            }
        }

        //받은적 없는 아이탬이면 여기로 내려옴.
        rewards.Add(new InvenSlot(index, amount));
    }

    public void StageClear()
    {
        for (int i = 0; i<rewards.Count; i++) // 보상 실제 지급.
        {
            PlayerData.Instance.GiveItem(rewards[i]._itemCode, rewards[i]._amount);
        }

        GameManager.Instance.stageInfoList[GameManager.Instance.currentStage - 1].clearCount++;

        StartCoroutine(StageClearCor());
    }

    IEnumerator StageClearCor()
    {
        UI_inGame.Instance.playerUI.SetActive(false);
        Time.timeScale = 0.2f;
        yield return new WaitForSeconds(0.5f);
        Time.timeScale = 1f;
        yield return new WaitForSeconds(1.5f);
        UI_inGame.Instance.ShowRewardPanel();
    }
}
