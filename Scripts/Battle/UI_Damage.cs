﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_Damage : MonoBehaviour
{
    static UI_Damage instance;
    public static UI_Damage Instance { get { return instance; } }

    public GameObject damageCanvasPF;
    public GameObject[] damageCanvas;
    int index = 0;

    public Vector3 offset = new Vector3(0, 1, 0);

    void Awake()
    {
        instance = this;

        for(int i = 0; i<damageCanvas.Length; i++)
        {
            damageCanvas[i] = (GameObject)Instantiate(damageCanvasPF, transform);
        }
    }

    public void ShowCanvas(Vector3 position, int dmg, bool left, Color fontColor)
    {
        index++;
        if (index > damageCanvas.Length - 1) index = 0;

        damageCanvas[index].SetActive(false);

        damageCanvas[index].transform.position = position + offset;
        damageCanvas[index].transform.GetChild(0).GetComponentInChildren<Text>().text = dmg.ToString();
        damageCanvas[index].transform.GetChild(0).GetComponentInChildren<Text>().color = fontColor;

        damageCanvas[index].SetActive(true);

        if(left)
        {
            damageCanvas[index].GetComponentInChildren<Animator>().Play("Left");
        }
        else
        {
            damageCanvas[index].GetComponentInChildren<Animator>().Play("Right");
        }
    }
}
