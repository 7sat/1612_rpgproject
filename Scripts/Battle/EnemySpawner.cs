﻿using UnityEngine;
using System.Collections;

public class EnemySpawnInfo
{
    public int[] EnemyType;
    public int[] EnemyAmount;

    public EnemySpawnInfo (int[] Type, int[] Amount)
    {
        EnemyType = Type;
        EnemyAmount = Amount;
    }
}

public class EnemySpawner : MonoBehaviour
{
    static EnemySpawner instance;
    public static EnemySpawner Instance { get { return instance; } }

    public GameObject[] enemyPF;

    public EnemySpawnInfo[] spawnInfo = new EnemySpawnInfo[3];

    void Awake()
    {
        instance = this;
    }

    public void Setup()
    {
        spawnInfo[0] = new EnemySpawnInfo(new int[]{ 0 }, new int[] { 1 });
        spawnInfo[1] = new EnemySpawnInfo(new int[] { 0, 1 }, new int[] { 1, 0 });
        spawnInfo[2] = new EnemySpawnInfo(new int[] { 0, 1, 2 }, new int[] { 1, 0, 0 });

        SpawnEnemy(0);
    }

    public void SpawnEnemy(int Index)
    {
        SectionManager.Instance.enemyLeft = 0;

        for (int t = 0; t<spawnInfo[Index].EnemyType.Length; t++) // 등장하는 몹 종류 수 만큼 반복.
        {
            for(int a = 0; a<spawnInfo[Index].EnemyAmount[t]; a++) // 해당 유형 몹의 등장 수량만큼 반복.
            {
                Vector3 offset = new Vector3(Random.Range(-2f, 2f), 0, Random.Range(-2f, 2f));

                Instantiate(enemyPF[spawnInfo[Index].EnemyType[t]],
                            SectionManager.Instance.spawnPos[Index].transform.position + offset,
                            Quaternion.identity);

                SectionManager.Instance.enemyLeft++;
            }
        }
    }
}
