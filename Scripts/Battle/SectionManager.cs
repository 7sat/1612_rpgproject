﻿using UnityEngine;
using System.Collections;

public class SectionManager : MonoBehaviour
{
    static SectionManager instance;
    public static SectionManager Instance { get { return instance; } }

    public int currentSection = 1;
    public GameObject sectionStartPF;
    public GameObject[] sectionStartPos;

    public bool camLerp = false;

    public GameObject spawnerPF; // 스포너 오브젝트.
    public Transform[] spawnPos; // 섹션별 적 스폰위치.
    [HideInInspector] public int enemyLeft; // 현 섹션에서 남은 적 유닛 수.

    void Awake()
    {
        instance = this;
    }
    
    public void Setup()
    {
        sectionStartPos = new GameObject[StageManager.Instance.totalSection + 1];
        for (int i = 0; i < StageManager.Instance.totalSection + 1; i++)
        {
            sectionStartPos[i] = (GameObject)Instantiate(sectionStartPF, new Vector3(0, 0, i * 20), Quaternion.identity, transform);
        }

        spawnPos = new Transform[StageManager.Instance.totalSection];
        for (int i = 0; i < StageManager.Instance.totalSection; i++)
        {
            GameObject go = (GameObject)Instantiate(spawnerPF, new Vector3(0, 0, i * 20 + 15), Quaternion.identity, transform);
            spawnPos[i] = go.transform;
        }

        UI_inGame.Instance.UpdateSectionText();
    }

    public float leftEnd()
    {
        return sectionStartPos[0].transform.position.z;
    }

    public float rightEnd()
    {
        return sectionStartPos[currentSection].transform.position.z;
    }

    public void GoToNextSection()
    {
        currentSection++;

        UI_inGame.Instance.UpdateSectionText();

        EnemySpawner.Instance.SpawnEnemy(currentSection-1);

        StartCoroutine(CamLerpCor());
    }

    IEnumerator CamLerpCor()
    {
        camLerp = true;
        yield return new WaitForSeconds(0.2f); // 수정필요.
        while (CamMove.Instance.camPlayerDist > 0.2f)
        {
            yield return null;
        }
        camLerp = false;
    }

    public void EnemyKilled()
    {
        enemyLeft--;
        if (enemyLeft <= 0)
        {
            if (currentSection == StageManager.Instance.totalSection) // 스테이지 클리어.
            {
                StageManager.Instance.StageClear();
            }
            else
            {
                GoToNextSection();
            }
        }
    }
}
