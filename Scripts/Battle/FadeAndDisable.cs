﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeAndDisable : MonoBehaviour
{
    public float time;
    public Text text;

    Color transparent = new Color(0,0,0,0);
    float t;

    void Awake()
    {
        text = transform.GetChild(0).GetComponentInChildren<Text>();
    }

	void OnEnable()
    {
        Invoke("DisableThis", time);
        t = -0.2f;
    }

    void DisableThis()
    {
        gameObject.SetActive(false);
    }

    void Update()
    {
        text.color = Color.Lerp(text.color, transparent, t);
        t += Time.deltaTime;
    }
}
