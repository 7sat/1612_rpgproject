﻿using UnityEngine;
using System.Collections;

public class BGMove : MonoBehaviour
{
    static BGMove instance;
    public static BGMove Instance { get { return instance; } }

    public GameObject[] bg = new GameObject[3]; // 0 원경, 1 중경, 2 근경
	public float[] spd = new float[3]; // 0에서 1사이. 0 = 전혀 안움직임. 1 = 바닥과 똑같이 움직임.

    void Awake()
    {
        instance = this;
    }

    public void Setup()
    {
        for (int i = 0; i < 3; i++) // 원경 중경 근경.
        {
            foreach (Transform t in transform.GetChild(i)) // 각 레이어의 모든 자식.
            {
                t.GetComponent<SpriteRenderer>().sprite = StageManager.Instance.bgImg[i];
            }
        }
    }

	void Update () 
	{
        if (CamMove.Instance.camClamped || StageManager.playerObj == null) return; // 카메라가 멈추면 배경도 멈춤.

		float h = Player.Instance.inputH;
        float PlayerSpd = StatInfo.moveSpd[Player.Instance.stat.moveSpd] * 0.1f;

		for (int i = 0; i < 3; i++) 
		{
			bg[i].transform.Translate (new Vector3 (-h * spd[i] * PlayerSpd * Time.deltaTime, 0, 0));
		}
	}
}
