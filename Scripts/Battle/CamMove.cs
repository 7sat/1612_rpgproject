﻿using UnityEngine;
using System.Collections;

public class CamMove : MonoBehaviour 
{
    static CamMove instance;
    public static CamMove Instance { get { return instance; } }

    void Awake()
    {
        instance = this;
    }

    float camSpd = 7;
	public Vector3 offset; // 카메라가 플레이어에게서 떨어진 정도.
    public float sectionEdgeOffset; // 섹션 가장자리 오프셋. 
    public float camPlayerDist; // lerp할때 카메라랑 플레이어 사이 거리.
    public bool camClamped = false; // 참이면 BGMove에서 뒷배경이 안움직임.

    void Update () 
	{
        if(Player.Instance == null)
        {
            return;
        }

        Vector3 targetPos = new Vector3(offset.x, offset.y, Player.Instance.gameObject.transform.position.z);

        float oldValue = targetPos.z;
        targetPos.z = Mathf.Clamp(targetPos.z, SectionManager.Instance.leftEnd() + sectionEdgeOffset, SectionManager.Instance.rightEnd() - sectionEdgeOffset);
        camClamped = (oldValue == targetPos.z) ? false : true;

        if(SectionManager.Instance.camLerp)
        {
            transform.position = Vector3.Lerp(transform.position, targetPos, camSpd * Time.deltaTime);
            camPlayerDist = Vector3.Distance(transform.position, targetPos);
        }
        else
        {
            transform.position = targetPos;
        }
    }
}
