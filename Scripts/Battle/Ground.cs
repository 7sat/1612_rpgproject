﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Ground : MonoBehaviour
{
    public float tileLength = 10;
    public List<GameObject> tile;
	
	void Update ()
    {
        if (StageManager.playerObj == null) return;

	    if(StageManager.playerObj.transform.position.z > tile[tile.Count-2].transform.position.z)
        {
            //플레이어가 가장 오른쪽 타일보다 오른쪽에 있음.
            tile[0].transform.position += new Vector3(0, 0, tileLength * tile.Count);

            GameObject temp = tile[0];
            tile.RemoveAt(0);
            tile.Add(temp);
        }
        else if(StageManager.playerObj.transform.position.z < tile[1].transform.position.z)
        {
            //플레이어가 가장 왼쪽 타일보다 왼쪽에 있음.
            tile[tile.Count - 1].transform.position -= new Vector3(0, 0, tileLength * tile.Count);

            GameObject temp = tile[tile.Count - 1];
            tile.RemoveAt(tile.Count - 1);
            tile.Insert(0, temp);
        }
	}
}
