﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI_inGame : MonoBehaviour
{
    static UI_inGame instance;
    public static UI_inGame Instance { get { return instance; } }

    void Awake()
    {
        instance = this;

        PausePanelSetup();
    }

    public GameObject playerUI;

    #region 일시정지창

    public GameObject pausePanel;
    public Text paused_stageName;

    void PausePanelSetup()
    {
        paused_stageName.text = GameManager.Instance.stageInfoList[GameManager.Instance.currentStage-1].name;
    }

    public void ShowPausePanel()
    {
        Time.timeScale = 0;
        pausePanel.SetActive(true);
    }

    public void ExitBtnClick()
    {
        UI_ConfirmPanel.Instance.ShowConfirmPanel("정말로 나갈까요?");
        StartCoroutine(WaitForConfirm_Exit());
    }

    IEnumerator WaitForConfirm_Exit()
    {
        while(!UI_ConfirmPanel.confirmed && !UI_ConfirmPanel.canceled)
        {
            yield return null;
        }
        if(UI_ConfirmPanel.confirmed)
        {
            SceneManager.LoadScene(0);
            Time.timeScale = 1;
        }
    }

    public void ResumeBtnClick()
    {
        pausePanel.SetActive(false);
        Time.timeScale = GameManager.Instance.currentGameSpeed;
    }

    public void SettingBtnClick()
    {
        SettingPanel.Instance.ShowSettingPanel();
    }

    #endregion

    #region 섹션패널

    public Text sectionText;

    public void UpdateSectionText()
    {
        sectionText.text = SectionManager.Instance.currentSection + " / 3";
    }

    #endregion

    #region 보상 패널

    public GameObject rewardPanel;
    public Image rewardImage;
    public Text rewardAmount;
    int index = 0;
    float lastClick;

    public void ShowRewardPanel()
    {
        rewardPanel.SetActive(true);
        rewardImage.sprite = GameManager.itemInfoList[StageManager.Instance.rewards[index]._itemCode].icon;
        rewardAmount.text = "x" + StageManager.Instance.rewards[index]._amount;
    }

    public void ShowNextReward()
    {
        if (Time.time - lastClick < 0.5f) return;

        lastClick = Time.time;

        if (index == StageManager.Instance.rewards.Count - 1)
        {
            rewardPanel.SetActive(false);
            ShowStageClearPanel();
            return;
        }

        index++;
        rewardImage.GetComponent<Animator>().Play("reward");
        rewardImage.sprite = GameManager.itemInfoList[StageManager.Instance.rewards[index]._itemCode].icon;
        rewardAmount.text = "x" + StageManager.Instance.rewards[index]._amount;
    }

    #endregion

    #region 스테이지클리어 패널

    public GameObject stageClearPanel;

    public void ShowStageClearPanel()
    {
        stageClearPanel.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadNextStage()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void Exit()
    {
        SceneManager.LoadScene(0);
    }

    #endregion

    public Image hpBar;
    int maxHP;

    public Image[] coolTimeIndicator = new Image[5]; // 대쉬,패시브,액티브3개

    public void Setup()
    {
        maxHP = Mathf.RoundToInt(StatInfo.maxHP[Player.Instance.stat.maxHP]);
    }

    void Update()
    {
        if (StageManager.playerObj == null) return;

        UpdateHpBar();
        UpdateSkillUI();
    }

    void UpdateHpBar()
    {
        hpBar.fillAmount = Player.Instance.currentHP/maxHP;
    }

    void UpdateSkillUI()
    {
        for (int i = 0; i < 8; i++)
        {
            SlotInfo slotinfo = Player.Instance.playerSkills.slotInfo[i];

            if (slotinfo.skillIndex == -1)
            {
                coolTimeIndicator[i].fillAmount = 0;
                continue;
            }

            float cooltime = SkillInfo.cooltime[slotinfo.skillIndex] * 0.1f * slotinfo.cooltimeMultiplier;
            float timeleft = slotinfo.cooltimeLeft;
            coolTimeIndicator[i].fillAmount = (cooltime - timeleft) / cooltime;
        }
    }
}
