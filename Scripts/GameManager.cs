﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.IO;
using System.Collections.Generic;

public class StatInfo
{
    public static int[] maxHP;
    public static int[] moveSpd; // x 0.1
    public static int[] atkPower;
    public static int[] atkRate; // x 0.1
    public static int[] critDamage; // x 0.1
    public static int[] critRate;
    public static int[] debuffResistance;
    public static int[] acc;
    public static int[] def;
    public static int[] evade;
}

public class SkillInfo
{
    public static string[] name;
    public static int[] cooltime; // x 0.1
} 

public class EnchantInfo
{
    public static int[] enchantProbability = new int[25]; // 성공 확률
    public static int[] ingredientRequire = new int[25]; //필요한 광물 양
    public static int[] goldRequire = new int[25]; // 필요한 골드
}

[System.Serializable]
public class ItemInfo
{
    public string name;
    public int cost;
    public bool stack; // 한칸에 쌓을수 있는지
    public int stackMax; // 최대 몇개까지
    public Sprite icon;
}

public class StageInfo
{
    public string name;
    public string story;
    public int clearCount;
}

public class GameManager : MonoBehaviour
{
    static GameManager instance;
    public static GameManager Instance { get { return instance; } }

    [Range (0.0f, 2.0f)]
    public float currentGameSpeed;
    float oldValue;

    string jsonString = "";
    JsonData statJson;
    JsonData skillJson;
    JsonData enchantJson;
    JsonData itemJson;
    JsonData stageJson;

    public Sprite goldIcon;

    public static List<ItemInfo> itemInfoList = new List<ItemInfo>();

    public StageInfo[] stageInfoList = new StageInfo[15];

    public int[] expRequire;

    public int currentStage;

    void Awake()
    {
        if (!instance)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        currentGameSpeed = oldValue = 1;

        StartCoroutine(ReadJsonString("StatInfo"));
        StartCoroutine(ReadJsonString("SkillInfo"));
        StartCoroutine(ReadJsonString("EnchantInfo"));
        StartCoroutine(ReadJsonString("ItemInfo"));
        StartCoroutine(ReadJsonString("StageInfo"));
    }

    IEnumerator ReadJsonString(string fileName)
    {
        string filePath = Application.streamingAssetsPath + "/" + fileName + ".json";

        if (filePath.Contains("://")) //안드로이드.
        {
            WWW www = new WWW(filePath);
            yield return www;
            jsonString = www.text;
        }
        else //pc.
        {
            jsonString = File.ReadAllText(filePath);
        }

        SendMessage("Read"+fileName+"Json");
    }

    void ReadStatInfoJson()
    {
        statJson = JsonMapper.ToObject(jsonString);

        int count = statJson["maxHP"].Count;

        StatInfo.maxHP = new int[count];
        StatInfo.moveSpd = new int[count];
        StatInfo.atkPower = new int[count];
        StatInfo.atkRate = new int[count];
        StatInfo.critDamage = new int[count];
        StatInfo.critRate = new int[count];
        StatInfo.debuffResistance = new int[count];
        StatInfo.acc = new int[count];
        StatInfo.def = new int[count];
        StatInfo.evade = new int[count];

        for (int i = 0; i < count; i++)
        {
            StatInfo.maxHP[i] = int.Parse(statJson["maxHP"][i].ToString());
            StatInfo.moveSpd[i] = int.Parse(statJson["moveSpd"][i].ToString());
            StatInfo.atkPower[i] = int.Parse(statJson["atkPower"][i].ToString());
            StatInfo.atkRate[i] = int.Parse(statJson["atkRate"][i].ToString());
            StatInfo.critDamage[i] = int.Parse(statJson["critDamage"][i].ToString());
            StatInfo.critRate[i] = int.Parse(statJson["critRate"][i].ToString());
            StatInfo.debuffResistance[i] = int.Parse(statJson["debuffResistance"][i].ToString());
            StatInfo.acc[i] = int.Parse(statJson["acc"][i].ToString());
            StatInfo.def[i] = int.Parse(statJson["def"][i].ToString());
            StatInfo.evade[i] = int.Parse(statJson["evade"][i].ToString());
        }
    }

    void ReadSkillInfoJson()
    {
        skillJson = JsonMapper.ToObject(jsonString);

        int count = skillJson["name"].Count;

        SkillInfo.name = new string[count];
        SkillInfo.cooltime = new int[count];

        for (int i = 0; i < count; i++)
        {
            SkillInfo.name[i] = skillJson["name"][i].ToString();
            SkillInfo.cooltime[i] = int.Parse(skillJson["cooltime"][i].ToString());
        }
    }

    void ReadEnchantInfoJson()
    {
        enchantJson = JsonMapper.ToObject(jsonString);

        int count = enchantJson["enchantProbability"].Count;

        EnchantInfo.enchantProbability = new int[count];
        EnchantInfo.ingredientRequire = new int[count];
        EnchantInfo.goldRequire = new int[count];

        for (int i = 0; i < count; i++)
        {
            EnchantInfo.enchantProbability[i] = int.Parse(enchantJson["enchantProbability"][i].ToString());
            EnchantInfo.ingredientRequire[i] = int.Parse(enchantJson["ingredientRequire"][i].ToString());
            EnchantInfo.goldRequire[i] = int.Parse(enchantJson["goldRequire"][i].ToString());
        }
    }

    void ReadItemInfoJson()
    {
        itemJson = JsonMapper.ToObject(jsonString);

        int count = itemJson["Items"].Count;

        for (int i = 0; i < count; i++)
        {
            ItemInfo info = new ItemInfo();

            info.name = itemJson["Items"][i]["name"].ToString();
            info.cost = int.Parse(itemJson["Items"][i]["cost"].ToString());
            info.stack = bool.Parse(itemJson["Items"][i]["stack"].ToString());
            info.stackMax = int.Parse(itemJson["Items"][i]["stackMax"].ToString());
            info.icon = Resources.Load<Sprite>("Items/"+i);

            itemInfoList.Add(info);
        }
    }

    void ReadStageInfoJson()
    {
        stageJson = JsonMapper.ToObject(jsonString);

        //15개.
        for(int i = 0; i<15; i++)
        {
            stageInfoList[i] = new StageInfo();
            stageInfoList[i].name = stageJson["Stage"][i]["name"].ToString();
            stageInfoList[i].story = stageJson["Stage"][i]["story"].ToString();
            stageInfoList[i].clearCount = int.Parse(stageJson["Stage"][i]["clearCount"].ToString());
        }
    }

    void Update()
    {
        if(currentGameSpeed != oldValue)
        {
            oldValue = Time.timeScale = currentGameSpeed;
        }
    }

}
