﻿using UnityEngine;
using System.Collections;

public class animEventTest : MonoBehaviour
{
    Character character;

    void Awake()
    {
        character = transform.root.GetComponent<Character>();
    }

    public void attackAniEvent()
    {
        character.Attack();
    }

    public void hitAniEvent()
    {
        character.HitAniFinish();
    }
}
