﻿using UnityEngine;
using System.Collections;
using LitJson;
using System.IO;

[System.Serializable]
public class PC
{
    public int index;
    public string name;
    public int level;
    public int exp;
    public Armor[] armor = new Armor[6];
    public int[] skills = new int[6]; // 이 캐릭터가 쓸수있는 모든 스킬 인덱스.

    public int armorRank; //계산.
    public Stat stat = new Stat(); // 계산.
}

[System.Serializable]
public class Armor // 장비 정보
{
    public static string[] _gradeText = new string[] { "<color=grey>최하급</color>", "<color=black>하급</color>", "<color=green>중급</color>", "<color=blue>고급</color>", "<color=yellow>최고급</color>" };
    public static string[] _abilityName = new string[] 
    { "공격력 증가", "방어력 증가", "치명타 확률 증가", "치명타 데미지 증가",
        "최대체력 증가", "이동속도 증가", "명중률 증가", "회피율 증가"};

    public string _name; // 장비명 (무기, 상하의, 장갑, 신발, 링커)
    public int _enchant; // 강화 ( 0 ~ +5 )6단계. _제련
    public int _grade; // 등급 5단계. _승급
    public int _ingredient; // 강화에 필요한 재료(광물).
    public int[] _ability = new int[2]; // 능력 유형
    public int[] _abilityAmount = new int[2]; // 능력 수치
}

[System.Serializable]
public class InvenSlot
{
    public int _itemCode;
    public int _amount;

    public InvenSlot(int itemCode, int amount)
    {
        _itemCode = itemCode;
        _amount = amount;
    }
}

public class PlayerData : MonoBehaviour
{
    static PlayerData instance;
    public static PlayerData Instance { get { return instance; } }

    JsonData jsonData;

    public int money;

    public PC[] pc = new PC[3]; // 모든 플레이어블 캐릭터.
    public int selectedMainCharIndex; // 플레이어가 선택한 캐릭 인덱스. (주캐릭)
    public int selectedSubCharIndex;
    
    //슬롯 몇개 열렸는지. 아머랭크 계산 이후 결과값이 원래보다 크면 팝업과 함께 슬롯개방.

    public InvenSlot[] invenSlot = new InvenSlot[21];

    void Awake()
    {
        if (!instance)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        StartCoroutine(ReadPlayerDataJson());
        LoadInventory();
    }

    IEnumerator ReadPlayerDataJson()
    {
        string jsonString;

        string filePath = Application.streamingAssetsPath + "/CharData.json";

        if(filePath.Contains("://"))
        {
            WWW www = new WWW(filePath);
            yield return www;
            jsonString = www.text;
        }
        else
        {
            jsonString = File.ReadAllText(filePath);
        }

        jsonData = JsonMapper.ToObject(jsonString);

        for (int i = 0; i<pc.Length; i++)
        {
            pc[i] = new PC();
            pc[i].index = int.Parse(jsonData["Character"][i]["index"].ToString());
            pc[i].name = jsonData["Character"][i]["name"].ToString();
            pc[i].level = int.Parse(jsonData["Character"][i]["level"].ToString());
            pc[i].exp = int.Parse(jsonData["Character"][i]["exp"].ToString());

            for (int j = 0; j < 6; j++)
            {
                pc[i].armor[j] = new Armor();

                JsonData json = jsonData["Character"][i]["armor"][j];

                pc[i].armor[j]._name = json["name"].ToString();
                pc[i].armor[j]._enchant = int.Parse(json["enchant"].ToString());
                pc[i].armor[j]._grade = int.Parse(json["grade"].ToString());
                pc[i].armor[j]._ingredient = int.Parse(json["ingredient"].ToString());
                pc[i].armor[j]._ability[0] = int.Parse(json["ability"]["a"].ToString());
                pc[i].armor[j]._ability[1] = int.Parse(json["ability"]["b"].ToString());
                pc[i].armor[j]._abilityAmount[0] = int.Parse(json["abilityAmount"]["a"].ToString());
                pc[i].armor[j]._abilityAmount[1] = int.Parse(json["abilityAmount"]["b"].ToString());
            }

            for (int k = 0; k < 6; k++)
            {
                pc[i].skills[k] = int.Parse(jsonData["Character"][i]["skill"][k].ToString());
            }

            StatCalc(i);
        }
    }

    void StatCalc(int Index) // 레벨업 하면 다시 계산해야됨.
    {
        // 기본 스텟.
        pc[Index].stat.Dex = pc[Index].level;
        pc[Index].stat.Con = pc[Index].level;
        pc[Index].stat.Wil = pc[Index].level;
        pc[Index].stat.Agi = pc[Index].level;
        pc[Index].stat.Dst = pc[Index].level;
        // 세부 스텟.
        pc[Index].stat.maxHP = pc[Index].stat.Dex;
        pc[Index].stat.moveSpd = pc[Index].stat.Dex;
        pc[Index].stat.atkPower = pc[Index].stat.Dex;
        pc[Index].stat.atkRate = pc[Index].stat.Dex;
        pc[Index].stat.critDmg = pc[Index].stat.Dex;
        pc[Index].stat.critRate = pc[Index].stat.Dex;
        pc[Index].stat.debuffResist = pc[Index].stat.Dex;
        pc[Index].stat.acc = pc[Index].stat.Dex;
        pc[Index].stat.def = pc[Index].stat.Dex;
        pc[Index].stat.evade = pc[Index].stat.Dex;
    }

    void LoadInventory()
    {
        //어디선가 불러와야함. giveItem은 여기서 못씀.
        invenSlot[0] = new InvenSlot(0, 128);
        invenSlot[1] = new InvenSlot(0, 128);
        invenSlot[2] = new InvenSlot(1, 128);
        invenSlot[3] = new InvenSlot(2, 64);
        invenSlot[4] = new InvenSlot(3, 32);
        invenSlot[5] = new InvenSlot(4, 16);
        invenSlot[6] = new InvenSlot(5, 1);
        
        for (int i = 7; i<invenSlot.Length; i++)
        {
            invenSlot[i] = new InvenSlot(-1, 0);
        }
    }

    public void ArmorRankCalc()
    {
        int rank = 0;

        for(rank = 0; rank < 5; rank++) // break될때까지 랭크 올림.
        {
            bool b = true;

            for (int i = 0; i < 6; i++) // 파츠별.
            {
                if (pc[selectedMainCharIndex].armor[i]._grade < rank)
                {
                    b = false;
                    break;
                }
            }

            if (!b) break;
        }

        pc[selectedMainCharIndex].armorRank = rank - 1;
    }

    public void AddExp(int amount)
    {
        pc[selectedMainCharIndex].exp += amount;
        if (pc[selectedMainCharIndex].exp >= GameManager.Instance.expRequire[pc[selectedMainCharIndex].level + 1])
        {
            pc[selectedMainCharIndex].exp -= GameManager.Instance.expRequire[pc[selectedMainCharIndex].level + 1];
            pc[selectedMainCharIndex].level++;
        }
    }


    //테스트용.
    public int test_I;
    public int test_A;
    public void TestBtnClick()
    {
        GiveItem(test_I, test_A);
        HeroPanel.Instance.UpdateInventory();
    }

    // 아이탬을 보통 전장에서 얻고 결과창에서 보여줌.
    // 꽉 차면?? 뭘 버리고 받던가 하려면 거기서도 인벤이 접근되야 하는거 아닌가.
    public void GiveItem(int index, int amount)
    {
        int amountLeft = amount;

        if (GameManager.itemInfoList[index].stack) // 주려는 아이탬이 스택 가능함?
        {
            int stackMax = GameManager.itemInfoList[index].stackMax;

            for (int i = 0; i < invenSlot.Length; i++) // 인벤토리 각 칸.
            {
                if (invenSlot[i]._itemCode == index) // 동일 아이탬 소지중?
                {
                    if (invenSlot[i]._amount <= stackMax - amountLeft) // 그 칸에 다 쌓을 수 있음.
                    {
                        invenSlot[i]._amount += amountLeft;
                        amountLeft = 0;
                        break; // 완료.
                    }
                    else if (invenSlot[i]._amount < stackMax) // 그 칸에 일부를 쌓을 수 있음.
                    {
                        amountLeft -= (stackMax - invenSlot[i]._amount);
                        invenSlot[i]._amount += (stackMax - invenSlot[i]._amount);
                    }
                }
            }

            if(amountLeft != 0)
            {
                for (int i = 0; i < invenSlot.Length; i++) // 인벤 각 칸.
                {
                    if(invenSlot[i]._itemCode == -1) // 비어있음.
                    {
                        invenSlot[i]._itemCode = index;

                        if (amountLeft > stackMax)
                        {
                            invenSlot[i]._amount += stackMax;
                            amountLeft -= stackMax;
                            continue;
                        }
                        else
                        {
                            invenSlot[i]._amount += amountLeft;
                            amountLeft = 0;
                            break; //완료
                        }
                    }
                }
                if(amountLeft != 0)
                {
                    print("인벤 공간 부족 : " + amountLeft + "개의 아이탬을 주지 못함.");
                }
            }
        }
        else
        {
            //스택 불가능한 아이탬 주기.
            for(int i = 0; i<invenSlot.Length; i++) // 인벤 각 칸.
            {
                if (invenSlot[i]._itemCode == -1) // 비어있음.
                {
                    invenSlot[i]._itemCode = index;
                    invenSlot[i]._amount += 1;
                    amountLeft -= 1;

                    if(amountLeft == 0) break; //완료
                }
            }
            if (amountLeft != 0)
            {
                print("인벤 공간 부족");
            }
        }
    }
}
