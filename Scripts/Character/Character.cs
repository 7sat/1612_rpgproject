﻿using UnityEngine;
using System.Collections;

public enum STATE { Idle, Move, Dash, Attack, Hit}

[System.Serializable]
public class Stat
{
    [Header(" - 기본 스탯")]
    public int Dex; // 재주. ~명중
    public int Con; // 건강. ~이상 저항
    public int Wil; // 의지. ~HP
    public int Agi; // 민첩. ~회피
    public int Dst; // 차원력. ~공격력,방어력
    [Header(" - 세부 스탯")]
    public int maxHP;
    public int moveSpd;
    public int atkPower;
    public int atkRate;
    public int critDmg;
    public int critRate;
    public int debuffResist; // 이상 저항. 사용안함.
    public int acc; // 명중. 사용안함.
    public int def; // 방어. 사용안함.
    public int evade; // 회피. 사용안함.
}

public class DamageInfo
{
    public bool left;
    public bool critical;
    public float damage;

    public DamageInfo(bool inLeft, bool inCritical, float inDamage)
    {
        left = inLeft; critical = inCritical; damage = inDamage;
    }
}

public class Character : MonoBehaviour
{
    [HideInInspector] public Animator anim;

    public bool invincible;
    public bool dead;
    public float currentHP;
    public STATE state; // 상태 (이동,공격,사망 등)
    public Stat stat = new Stat(); // 스텟 (공격력 등)

    public bool lookLeft = false; // 참이면 왼쪽을 바라보는중

    public Transform hitTf; // 스피어캐스트 원점
    public LayerMask enemyLayer;
    public int atkIndex = 1; // 평타 인덱스

    public virtual void Awake()
    {
        state = STATE.Idle;
        anim = GetComponentInChildren<Animator>();

        Initiate();
    }

    void Initiate()
    {
        if(tag == "Player")
        {
            stat = PlayerData.Instance.pc[PlayerData.Instance.selectedMainCharIndex].stat;
        }
        currentHP = StatInfo.maxHP[stat.maxHP];
    }

    public void Attack()
    {
        float damage = StatInfo.atkPower[stat.atkPower];
        bool critical = (Random.Range(0, 100) < StatInfo.critRate[stat.critRate]) ? true : false;
        if (critical) damage *= StatInfo.critDamage[stat.critDmg] * 0.1f;

        DamageInfo dInfo = new DamageInfo(lookLeft, critical, damage);

        Collider[] colliders = Physics.OverlapSphere(hitTf.position, 3.5f, enemyLayer);
        foreach(Collider col in colliders)
        {
            col.gameObject.SendMessage("GetDamage", dInfo);
        }
    }

    public bool isAttack() // Transition Duration 이 0이 아니면 제때 반응하지 않음.
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Run") ||
           anim.GetCurrentAnimatorStateInfo(0).IsName("Hit") ||
           anim.GetCurrentAnimatorStateInfo(0).IsName("Down") ||
           anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            print("isAtk False");
            return false;
        }
        else
        {
            return true; // 평타 또는 스킬 사용중.
        }
    }

    void GetDamage(DamageInfo dInfo)
    {
        if (invincible || dead) return;

        currentHP -= dInfo.damage;

        Color textColor = (dInfo.critical) ? Color.yellow : Color.white;
        UI_Damage.Instance.ShowCanvas(transform.position, Mathf.RoundToInt(dInfo.damage), dInfo.left, textColor);

        if (currentHP <= 0)
        {
            Die();
        }
        else
        {
            state = STATE.Hit;
            anim.SetTrigger("Hit");
        }
    }

    public void HitAniFinish() // Hit 애니메이션이 끝나면 실행됨.
    {
        state = STATE.Idle;
    }
    
    public virtual void Die()
    {
        dead = true;
        anim.SetTrigger("Down");
        Destroy(gameObject, 3f);
    }
}
