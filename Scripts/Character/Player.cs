﻿using UnityEngine;
using System.Collections;
using CnControls;

public class Player : Character
{
    static Player instance;
    public static Player Instance { get { return instance; } }

    [HideInInspector] public Skills playerSkills;

    [HideInInspector] public float inputH;
    [HideInInspector] public float inputV;
    float atkIndexResetCool;

    float lastAttack;

    public override void Awake()
    {
        base.Awake();
        instance = this;
        playerSkills = GetComponent<Skills>();
    }

    void Update()
    {
        MovePlayer();

        TryToAttack();
        TryToUseSkill();
        TryToDash();
    }

    void MovePlayer()
    {
        inputH = CnInputManager.GetAxisRaw("Horizontal");
        inputV = CnInputManager.GetAxisRaw("Vertical");

        if (isAttack() || state == STATE.Dash || dead)
        {
            inputH = inputV = 0;
        }

        if (inputH > 0)
        {
            transform.GetChild(0).rotation = new Quaternion(0, 0, 0, 0);
            lookLeft = false;
        }
        else if (inputH < 0)
        {
            transform.GetChild(0).rotation = new Quaternion(0, 180, 0, 0);
            lookLeft = true;
        }

        transform.Translate(new Vector3(-inputV, 0, inputH) * StatInfo.moveSpd[stat.moveSpd] * 0.1f * Time.deltaTime);

        float clampedX = Mathf.Clamp(transform.position.x, -4, 4);
        float clampedZ = Mathf.Clamp(transform.position.z, SectionManager.Instance.leftEnd() + 1, SectionManager.Instance.rightEnd() - 1);
        transform.position = new Vector3(clampedX, transform.position.y, clampedZ); // 캐릭터가 현재 구역을 벗어날 수 없음.

        bool move = (inputH != 0 || inputV != 0) ? true : false;
        anim.SetBool("Move", move); // 키 입력이 있으면 달리기 애니메이션 재생.
    }

    void TryToAttack()
    {
        if (CnInputManager.GetButtonDown("Attack"))
        {
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Attack3")) return;

            anim.SetTrigger("Attack");
        }
    }

    void TryToUseSkill()
    {
        int slotSelected = 0;
        if (CnInputManager.GetButtonDown("Passive")) slotSelected = 1;
        else if (CnInputManager.GetButtonDown("Skill1")) slotSelected = 2;
        else if (CnInputManager.GetButtonDown("Skill2")) slotSelected = 3;
        else if (CnInputManager.GetButtonDown("Skill3")) slotSelected = 4;
        else if (CnInputManager.GetButtonDown("Skill4")) slotSelected = 5;
        else if (CnInputManager.GetButtonDown("Skill5")) slotSelected = 6;
        else if (CnInputManager.GetButtonDown("Skill6")) slotSelected = 7;

        if (slotSelected != 0)
        {
            if(playerSkills.slotInfo[slotSelected].skillIndex == -1)
            {
                print("슬롯이 비어있음");
                return;
            }
            else
            {
                playerSkills.UseSkill(slotSelected);
            }
        }
    }

    void TryToDash()
    {
        if(CnInputManager.GetButtonDown("Evasion") && state != STATE.Dash) // 회피키 누름.
        {
            if(!lookLeft) // 오른쪽 보고있음.
            {
                if (transform.position.z < SectionManager.Instance.rightEnd() - 2) // 오른쪽으로 여유공간 있음.
                {
                    playerSkills.UseDash(1);
                }
            }
            else
            {
                if(transform.position.z > SectionManager.Instance.leftEnd() + 2)
                {
                    playerSkills.UseDash(-1);
                }
            }
        }
    }
}
