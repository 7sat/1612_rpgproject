﻿using UnityEngine;
using System.Collections;

public class Enemy : Character
{
    float distFromPlayer;
    NavMeshAgent navMeshAgent;

    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.speed = StatInfo.moveSpd[stat.moveSpd] * 0.1f;
        StartCoroutine(StateCor());
    }

    IEnumerator StateCor()
    {
        while(!dead && !Player.Instance.dead)
        {
            if (state == STATE.Hit) // 피격 애니메이션 재생 중에는 아무것도 하지 않음.
            {
                navMeshAgent.Stop();
                yield return null;
                continue;
            }

            distFromPlayer = Vector3.Distance(transform.position, StageManager.playerObj.transform.position);

            if (distFromPlayer > 9) // 플레이어가 시야 밖에 있음.
            {
                state = STATE.Idle;
                navMeshAgent.Stop();
                anim.SetBool("Move", false);
            }
            else
            {
                LookatPlayer();

                if (distFromPlayer > 3f) //플레이어가 사거리 밖에 있음.
                {
                    state = STATE.Move;

                    if(!anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                    {
                        navMeshAgent.Resume();
                        navMeshAgent.SetDestination(StageManager.playerObj.transform.position);
                        anim.SetBool("Move", true);
                    }
                }
                else // 공격 코루틴 시작.
                {
                    if(state != STATE.Attack)
                    {
                        state = STATE.Attack;
                        navMeshAgent.Stop();
                        anim.SetBool("Move", false);

                        StopCoroutine("AtkCor");
                        StartCoroutine("AtkCor");
                    }
                }
            }
            yield return new WaitForSeconds(Random.Range(0.2f, 0.3f));
        }
        //플레이어가 죽었거나 null임.
        state = STATE.Idle;
        navMeshAgent.Stop();
    }

    IEnumerator AtkCor()
    {
        while(state == STATE.Attack)
        {
            anim.SetTrigger("Attack");
            yield return new WaitForSeconds(StatInfo.atkRate[stat.atkRate] / 10f);
        }
    }

    void LookatPlayer()
    {
        if (transform.position.z < StageManager.playerObj.transform.position.z)
        {
            transform.GetChild(0).rotation = new Quaternion(0, 0, 0, 0);
            lookLeft = false;
        }
        else if (transform.position.z > StageManager.playerObj.transform.position.z)
        {
            transform.GetChild(0).rotation = new Quaternion(0, 180, 0, 0);
            lookLeft = true;
        }
    }

    public override void Die()
    {
        navMeshAgent.Stop();

        int reward = Random.Range(0, 4);
        int amount = Random.Range(1, 4);
        StageManager.Instance.AddReward(reward, amount);

        SectionManager.Instance.EnemyKilled();

        base.Die();
    }
}
