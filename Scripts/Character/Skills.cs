﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SlotInfo
{
    public bool locked;
    public int skillIndex;
    public float cooltimeLeft;
    public float cooltimeMultiplier;

    public SlotInfo (bool inLocked, int inSkillIndex, float inCooltimeLeft, float inCooltimeMultiplier)
    {
        locked = inLocked;
        skillIndex = inSkillIndex;
        cooltimeLeft = inCooltimeLeft;
        cooltimeMultiplier = inCooltimeMultiplier;
    }
}

public class Skills : MonoBehaviour
{
    Character character;
    public SlotInfo[] slotInfo = new SlotInfo[8]; // 대쉬, 패시브, 1,2,3,4,5,6 // 인스펙터에서 넣고있음.

    void Awake()
    {
        character = GetComponent<Character>();
    }

    void Update()
    {
        CoolTimeCalc();
    }

    void CoolTimeCalc()
    {
        for(int i = 0; i<8; i++)
        {
            if (slotInfo[i].skillIndex == -1) continue; // 스킬 미등록.

            if (slotInfo[i].cooltimeLeft > 0)
            {
                slotInfo[i].cooltimeLeft -= Time.deltaTime;
            }
        }
    }

    public void UseDash(int dir)
    {
        if (character.isAttack()) return; // 공격중임.
        if (character.state == STATE.Dash) return; // 대쉬중임.
        if (slotInfo[0].cooltimeLeft > 0) return; // 쿨타임 남음.

        StartCoroutine(DashCor(dir));
    }

    public IEnumerator DashCor(int dir)
    {
        character.invincible = true;
        character.anim.Play("Idle");
        slotInfo[0].cooltimeLeft = SkillInfo.cooltime[0] * 0.1f * slotInfo[0].cooltimeMultiplier;
        character.state = STATE.Dash;

        float f = 0;
        while (f < 2)
        {
            transform.position += new Vector3(0, 0, 25 * dir * Time.deltaTime);
            f += 20 * Time.deltaTime;
            yield return null;
        }

        character.invincible = false;
        character.state = STATE.Idle;
    }

    public void UseSkill(int index) // index는 슬롯의 인덱스임.
    {
        if (character.isAttack())
        {
            print("isAtk : true");
            return; // 공격중임.
        }

        if (character.state == STATE.Dash) return; // 대쉬중임.
        if (slotInfo[index].cooltimeLeft > 0) return; // 쿨타임 남음.

        int skillIndex = slotInfo[index].skillIndex;

        slotInfo[index].cooltimeLeft = SkillInfo.cooltime[skillIndex] * 0.1f * slotInfo[index].cooltimeMultiplier;
        StartCoroutine(SkillInfo.name[skillIndex] + "Cor");
    }

    IEnumerator PassiveCor() // 테스트용으로 3연타 넣었는데 이제 안씀.
    {
        for (int i = 0; i < 3; i++)
        {
            character.anim.SetTrigger("Attack1");

            yield return new WaitForSeconds(0.2f);
        }
    }

    IEnumerator Active1Cor()
    {
        character.anim.SetTrigger("Skill1");
        yield return null;
    }

    IEnumerator Active2Cor()
    {
        character.anim.SetTrigger("Skill2");
        yield return null;
    }

    IEnumerator Active3Cor()
    {
        character.anim.SetTrigger("Skill3");
        yield return null;
    }

    IEnumerator Active4Cor()
    {
        character.anim.SetTrigger("Skill4");
        yield return null;
    }

    IEnumerator Active5Cor()
    {
        character.anim.SetTrigger("Skill5");
        yield return null;
    }

    IEnumerator Active6Cor()
    {
        character.anim.SetTrigger("Skill6");
        yield return null;
    }
}
