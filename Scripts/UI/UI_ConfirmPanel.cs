﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_ConfirmPanel : MonoBehaviour
{
    static UI_ConfirmPanel instance;
    public static UI_ConfirmPanel Instance { get { return instance; } }

    void Awake()
    {
        if (!instance)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public GameObject confirmPanel;
    public Text confirmPanelText;

    public static bool confirmed; // 확인을 누름.
    public static bool canceled; // 취소를 누름.

    public void ShowConfirmPanel(string msg) // 확인 창 표시
    {
        confirmed = false;
        canceled = false;
        confirmPanel.SetActive(true);
        confirmPanelText.text = msg;
    }

    public void ConfirmBtnClick() // 확인
    {
        confirmPanel.SetActive(false);
        confirmed = true;
    }

    public void CancelBtnClick() // 취소
    {
        confirmPanel.SetActive(false);
        canceled = true;
    }
}
