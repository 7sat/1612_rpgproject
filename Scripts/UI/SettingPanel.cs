﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettingPanel : MonoBehaviour
{
    static SettingPanel instance;
    public static SettingPanel Instance { get { return instance; } }

    public GameObject settingPanel;
    public Slider bgmSlider;
    public Slider sfxSlider;
    public Toggle bgmMuteToggle;
    public Toggle sfxMuteToggle;

    public float bgmVolume = 1f;
    public float sfxVolume = 1f;

    public bool isBgmMute = false;
    public bool isSfxMute = false;

    void Awake()
    {
        if (!instance)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public void ShowSettingPanel()
    {
        settingPanel.SetActive(true);
    }

    public void HideSettingPanel()
    {
        settingPanel.SetActive(false);
    }

    public void SetBGMVolume()
    {
        bgmVolume = bgmSlider.value;
        print("bgm " + bgmVolume);
    }

    public void SetSFXVolume()
    {
        sfxVolume = sfxSlider.value;
        print("sfx " + sfxVolume);
    }

    public void toggleBgmMute()
    {
        isBgmMute = bgmMuteToggle.isOn;
        print("배경음 음소거 " + isBgmMute);
    }

    public void toggleSfxMute()
    {
        isSfxMute = sfxMuteToggle.isOn;
        print("효과음 음소거 " + isSfxMute);
    }
}
