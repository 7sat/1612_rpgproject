﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ErrorMsg : MonoBehaviour
{
    static ErrorMsg instance;
    public static ErrorMsg Instance { get { return instance; } }

    public GameObject errMsgPF;
    public Transform rootCanv;

    void Awake()
    {
        instance = this;
    }

	public void ShowErrMsg(string msg)
    {
        GameObject g = (GameObject)Instantiate(errMsgPF, rootCanv, false);
        g.GetComponentInChildren<Text>().text = msg;
    }
}
