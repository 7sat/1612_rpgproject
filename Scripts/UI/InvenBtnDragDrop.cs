﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InvenBtnDragDrop : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler
{
    Transform rootCanv;

    GameObject tempBtn;

    public int slotNum;

    void Start()
    {
        rootCanv = transform.parent.parent;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        tempBtn = (GameObject)Instantiate(gameObject, rootCanv);
        tempBtn.GetComponent<CanvasGroup>().blocksRaycasts = false;
        GetComponent<Button>().interactable = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        tempBtn.transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        GetComponent<Button>().interactable = true;
        Destroy(tempBtn);
    }

    public void OnDrop(PointerEventData eventData)
    {
        InvenBtnDragDrop d = eventData.pointerDrag.GetComponent<InvenBtnDragDrop>();
        if (d != null)
        {
            int tempI = PlayerData.Instance.invenSlot[slotNum]._itemCode;
            int tempA = PlayerData.Instance.invenSlot[slotNum]._amount;

            PlayerData.Instance.invenSlot[slotNum]._amount = PlayerData.Instance.invenSlot[d.slotNum]._amount;
            PlayerData.Instance.invenSlot[slotNum]._itemCode = PlayerData.Instance.invenSlot[d.slotNum]._itemCode;

            PlayerData.Instance.invenSlot[d.slotNum]._amount = tempA;
            PlayerData.Instance.invenSlot[d.slotNum]._itemCode = tempI;

            HeroPanel.Instance.UpdateInventory();
        }
    }
}
