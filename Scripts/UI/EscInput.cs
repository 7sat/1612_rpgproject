﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EscInput : MonoBehaviour
{
    public static bool disable;
    public static GameObject panel; // esc키에 반응할 패널.

    void Awake()
    {
        panel = MainMenu.Instance.startPanel;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (disable) return;

            if(panel == MainMenu.Instance.startPanel)
            {
                if (!UI_ConfirmPanel.Instance.confirmPanel.activeSelf)
                {
                    UI_ConfirmPanel.Instance.ShowConfirmPanel("게임을 종료 하시겠습니까?");
                    StartCoroutine(ExitCor());
                }
                else
                {
                    UI_ConfirmPanel.Instance.CancelBtnClick();
                }
            }
            else
            {
                panel.SendMessage("Escape");
            }
        }
    }

    IEnumerator ExitCor()
    {
        while (!UI_ConfirmPanel.confirmed && !UI_ConfirmPanel.canceled)
        {
            yield return null;
        }
        if (UI_ConfirmPanel.confirmed)
        {
            Application.Quit();
        }
    }
}
