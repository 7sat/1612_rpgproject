﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StagePanel : MonoBehaviour
{
    public GameObject stageInfoPanel;

    public Text stageName;
    public Text stageStory;

    public Text repeatCountText;
    public Image repeatCountBar;
    public GameObject repeatRewardBtn;

    public GameObject startBtn;

    public RectTransform content;
    int index;

    public GameObject[] stageBtn;

    public void Start()
    {
        CheckLock();
    }

    public void CheckLock()
    {
        for(int i = 0; i<14; i++)
        {
            if(GameManager.Instance.stageInfoList[i].clearCount > 0)
            {
                stageBtn[i + 1].GetComponent<Button>().interactable = true;
                stageBtn[i + 1].transform.GetChild(1).gameObject.SetActive(false);
            }
            else
            {
                return;
            }
        }
    }

    public void SelectStage(int num)
    {
        stageInfoPanel.SetActive(true);
        EscInput.panel = stageInfoPanel;

        stageName.text = GameManager.Instance.stageInfoList[num-1].name;
        stageStory.text = GameManager.Instance.stageInfoList[num-1].story;

        int count = GameManager.Instance.stageInfoList[num - 1].clearCount;
        repeatCountText.text = count + "/3";
        repeatCountBar.fillAmount = count / 3f;
        if(count>=3)
        {
            repeatRewardBtn.SetActive(true);
        }
        else
        {
            repeatRewardBtn.SetActive(false);
        }

        startBtn.GetComponent<Button>().onClick.RemoveAllListeners();
        startBtn.GetComponent<Button>().onClick.AddListener(() => LoadStage(num));
    }

    public void HideStageInfoPanel()
    {
        stageInfoPanel.SetActive(false);
        EscInput.panel = MainMenu.Instance.stagePanel;
    }

    #region 스테이지 로딩

    AsyncOperation ao;
    public GameObject loadingScreenBg;
    public Slider progBar;
    public Text loadingText;

    public void LoadStage(int num)
    {
        if (num > SceneManager.sceneCount) return;

        GameManager.Instance.currentStage = num;

        loadingScreenBg.SetActive(true);
        progBar.gameObject.SetActive(true);
        loadingText.gameObject.SetActive(true);
        loadingText.text = "로딩중...";

        StartCoroutine(LoadStageCor(num));
    }

    IEnumerator LoadStageCor(int num)
    {
        yield return new WaitForSeconds(0.5f);

        ao = SceneManager.LoadSceneAsync(num);
        ao.allowSceneActivation = false;

        while(!ao.isDone)
        {
            progBar.value = ao.progress;

            if(ao.progress == 0.9f)
            {
                progBar.value = 1f;
                //loadingText.text = "화면을 터치하세요";
                //if(Input.GetMouseButtonDown(0))
                //{
                //    ao.allowSceneActivation = true;
                //}

                ao.allowSceneActivation = true;
            }

            yield return null;
        }
    }

    #endregion

    #region 페널 좌우 이동

    public void LBtnClick()
    {
        index--;
        if (index < 0) index = 2;
        StagePanelLerp();
    }

    public void RBtnClick()
    {
        index++;
        if (index > 2) index = 0;
        StagePanelLerp();
    }

    void Update()
    {
        StagePanelLerp();
    }

    public void StagePanelLerp()
    {
        float newX = Mathf.Lerp(content.anchoredPosition.x, -1900*index, Time.deltaTime * 12f);
        content.anchoredPosition = new Vector2(newX, content.anchoredPosition.y);
    }

    #endregion
}
