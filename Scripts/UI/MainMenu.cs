﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    static MainMenu instance;
    public static MainMenu Instance { get { return instance; } }

    public GameObject startPanel;
    public GameObject heroPanel;
    public GameObject shopPanel;
    public GameObject questPanel;
    public GameObject stagePanel;

    public Text goldText;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        UpdateMoneyText();
    }

    public void ShowHeroPanel()
    {
        startPanel.SetActive(false);
        heroPanel.SetActive(true);
        HeroPanel.Instance.UpdateAll();

        EscInput.panel = heroPanel;
    }

    public void HideHeroPanel()
    {
        startPanel.SetActive(true);
        heroPanel.SetActive(false);

        EscInput.panel = startPanel;
    }

    public void ShowShopPanel()
    {
        startPanel.SetActive(false);
        shopPanel.SetActive(true);

        EscInput.panel = shopPanel;
    }

    public void HideShopPanel()
    {
        startPanel.SetActive(true);
        shopPanel.SetActive(false);

        EscInput.panel = startPanel;
    }

    public void ShowQuestPanel()
    {
        startPanel.SetActive(false);
        questPanel.SetActive(true);

        EscInput.panel = questPanel;
    }

    public void HideQuestPanel()
    {
        startPanel.SetActive(true);
        questPanel.SetActive(false);

        EscInput.panel = startPanel;
    }

    public void ShowStagePanel()
    {
        startPanel.SetActive(false);
        stagePanel.SetActive(true);

        EscInput.panel = stagePanel;
    }

    public void HideStagePanel()
    {
        startPanel.SetActive(true);
        stagePanel.SetActive(false);

        EscInput.panel = startPanel;
    }

    public void ShowSettingPanel()
    {
        SettingPanel.Instance.ShowSettingPanel();
    }

    public void UpdateMoneyText()
    {
        goldText.text = PlayerData.Instance.money.ToString();
    }
}
