﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HeroPanel : MonoBehaviour
{
    static HeroPanel instance;
    public static HeroPanel Instance { get { return instance; } }

    //페널
    public GameObject statPanel;
    public GameObject equipInfoPanel;
    public GameObject checkPanel;
    public GameObject resultPanel;

    //캐릭터
    public int selectedCardIndex;
    public Image selectedCharImg;
    public Sprite[] charImg = new Sprite[3];
    public Text charName, charLv;
    public GameObject charListPanel;
    public GameObject charCardPF;
    public GameObject[] charCard = new GameObject[3];

    //스탯
    public Text dex, con, wil, agi, dst;
    public Text maxhp, movespd, atkpower, atkrate, critdmg, critrate, debuffresist, acc, def, evade;

    //장비
    public Text armorRank;
    public GameObject[] equipBtn = new GameObject[6];
    Armor a;
    public Text equipName;
    public Text equipInfo;
    public Image equipIcon;
    public Text abilityA;
    public Text abilityB;
    public GameObject enchantBtn;
    public GameObject upgradeBtn;

    //강화, 승급 창
    public Text checkPanelTitle;
    public Image equipIcon_checkPanel;
    public Text equipName_checkPanel;
    public Text enchantRequirement;
    public Text before_stat1, before_stat2, before_stat3, after_stat1, after_stat2, after_stat3;
    public Text successRate;
    public GameObject enchantBtnB;
    public GameObject upgradeBtnB;

    //결과
    public Text resultText;
    public Image successEffect;
    public Image equipIcon_result;

    //인벤토리
    public Transform inventoryTF;
    public GameObject inventoryPF;
    public GameObject[] invenBtn = new GameObject[21];

    void Awake()
    {
        instance = this;

        for (int i = 0; i<21; i++)
        {
            invenBtn[i] = (GameObject)Instantiate(inventoryPF, inventoryTF, false);
            invenBtn[i].GetComponent<InvenBtnDragDrop>().slotNum = i;
        }
    }

    public void UpdateAll()
    {
        UpdateCharInfo();
        UpdateStatPanel();
        UpdateEquipPanel();
        UpdateInventory();
        UpdateArmorRankText();
        UpdateCharListPanel();
    }

    #region [[ 캐릭터와 스탯 ]]

    public void CharCardClick(int index)
    {
        selectedCardIndex = index;
        UpdateCharInfo();
        UpdateEquipPanel();
        UpdateStatPanel();
        HideEquipInfoPanel();
    }

    public void UpdateCharInfo()
    {
        charLv.text = "Lv." + PlayerData.Instance.pc[selectedCardIndex].level;
        charName.text = PlayerData.Instance.pc[selectedCardIndex].name;
        selectedCharImg.sprite = charImg[selectedCardIndex];
    }

    void UpdateStatPanel()
    {
        Stat s = PlayerData.Instance.pc[selectedCardIndex].stat;
        dex.text = "재주 " + s.Dex;
        con.text = "건강 " + s.Con;
        wil.text = "의지 " + s.Wil;
        agi.text = "민첩 " + s.Agi;
        dst.text = "차원력 " + s.Dst;
        maxhp.text = "최대체력 " + s.maxHP;
        movespd.text = "이동속도 " + s.moveSpd;
        atkpower.text = "공격력 " + s.atkPower;
        atkrate.text = "공격속도 " + s.atkRate;
        critdmg.text = "치명타공격력 " + s.critDmg;
        critrate.text = "치명타확률 " + s.critRate;
        debuffresist.text = "이상저항 " + s.debuffResist;
        acc.text = "명중 " + s.acc;
        def.text = "방어 " + s.def;
        evade.text = "회피 " + s.evade;
    }

    public void SetMainChar()
    {
        if(PlayerData.Instance.selectedSubCharIndex == selectedCardIndex)
        {
            ErrorMsg.Instance.ShowErrMsg("보조 캐릭터로 사용중입니다.");
            return;
        }

        PlayerData.Instance.selectedMainCharIndex = selectedCardIndex;
        UpdateCharListPanel();
    }

    public void SetSubChar()
    {
        if (PlayerData.Instance.selectedMainCharIndex == selectedCardIndex)
        {
            ErrorMsg.Instance.ShowErrMsg("메인 캐릭터로 사용중입니다.");
            return;
        }

        PlayerData.Instance.selectedSubCharIndex = selectedCardIndex;
        UpdateCharListPanel();
    }

    void UpdateCharListPanel()
    {
        foreach(Transform t in charListPanel.transform)
        {
            Destroy(t.gameObject);
        }

        charCard = new GameObject[PlayerData.Instance.pc.Length];

        for(int i = 0; i<PlayerData.Instance.pc.Length; i++)
        {
            charCard[i] = (GameObject)Instantiate(charCardPF, charListPanel.transform);
            charCard[i].GetComponent<Image>().sprite = charImg[PlayerData.Instance.pc[i].index];

            charCard[i].transform.GetChild(1).GetComponent<Text>().text = PlayerData.Instance.pc[i].level.ToString();

            GameObject nameTag = charCard[i].transform.GetChild(0).gameObject;
            nameTag.SetActive(true);

            if (i == PlayerData.Instance.selectedMainCharIndex)
            {
                nameTag.GetComponentInChildren<Text>().text = "메인";
            }
            else if(i == PlayerData.Instance.selectedSubCharIndex)
            {

                nameTag.GetComponentInChildren<Text>().text = "보조";
            }
            else
            {
                nameTag.gameObject.SetActive(false);
            }

            int index = i;
            charCard[i].GetComponent<Button>().onClick.AddListener(() => CharCardClick(index));
        }
    }

    #endregion

    #region [[ 강화와 승급 ]]

    public void ShowCheckPanel()
    {
        checkPanel.SetActive(true);
        UpdateCheckPanel();
        EscInput.panel = checkPanel;
    }

    void UpdateCheckPanel()
    {
        int x = (a._grade * 5) + a._enchant;
        if (x >= 25)
        {
            checkPanel.SetActive(false);
            return;
        }

        equipIcon_checkPanel.sprite = equipIcon.sprite;
        equipName_checkPanel.text = a._name + " (" + Armor._gradeText[a._grade] + " +" + a._enchant + ")";

        if (a._enchant >= 5) // 승급
        {
            checkPanelTitle.text = "승급";
            upgradeBtnB.SetActive(true);
            enchantBtnB.SetActive(false);
            enchantRequirement.text = "(승급요구사항)";
            successRate.text = "100%";
            before_stat1.text = Armor._abilityName[a._ability[0]] + ": +" + a._abilityAmount[0];
            before_stat2.text = Armor._abilityName[a._ability[1]] + ": +" + a._abilityAmount[1];
            before_stat3.text = "";
            after_stat1.text = Armor._abilityName[a._ability[0]] + ": <color=green>+" + (a._abilityAmount[0] + 4) + "</color>";
            after_stat2.text = Armor._abilityName[a._ability[1]] + ": <color=green>+" + (a._abilityAmount[1] + 2) + "</color>";
            after_stat3.text = "";
        }
        else // 강화.
        {
            checkPanelTitle.text = "강화";
            upgradeBtnB.SetActive(false);
            enchantBtnB.SetActive(true);
            enchantRequirement.text = EnchantInfo.goldRequire[x] + "골드, " + GameManager.itemInfoList[a._ingredient].name + " " + EnchantInfo.ingredientRequire[x] + "개";
            successRate.text = EnchantInfo.enchantProbability[x] + "%";
            before_stat1.text = Armor._abilityName[a._ability[0]] + ": +" + a._abilityAmount[0];
            before_stat2.text = Armor._abilityName[a._ability[1]] + ": +" + a._abilityAmount[1];
            before_stat3.text = "";
            after_stat1.text = Armor._abilityName[a._ability[0]] + ": <color=green>+" + (a._abilityAmount[0] + 1) + "</color>";
            after_stat2.text = Armor._abilityName[a._ability[1]] + ": +" + a._abilityAmount[1];
            after_stat3.text = "";
        }
    }

    public void HideCheckPanel()
    {
        checkPanel.SetActive(false);
        EscInput.panel = equipInfoPanel;
    }

    public void EnchantBtnClick()
    {
        int x = (a._grade * 5) + a._enchant;

        if (PlayerData.Instance.money < EnchantInfo.goldRequire[x])
        {
            //resultText.text = "골드가 부족합니다.";
            ErrorMsg.Instance.ShowErrMsg("골드가 부족합니다.");
            return;
        }
        else
        {
            PlayerData.Instance.money -= EnchantInfo.goldRequire[x]; // 골드 소모.
            MainMenu.Instance.UpdateMoneyText();
        }

        int Require = EnchantInfo.ingredientRequire[x]; // 재료가 필요한 만큼 있는지 먼저 확인함.

        for (int index = 0; index < PlayerData.Instance.invenSlot.Length; index++) // 인벤 각 칸.
        {
            if (PlayerData.Instance.invenSlot[index]._itemCode == a._ingredient) // 재료 아이탬 찾음.
            {
                Require -= PlayerData.Instance.invenSlot[index]._amount;
                if (Require <= 0) break; // 재료가 필요한 만큼 있음.
            }
        }

        if(Require > 0)//재료 부족함. 종료.
        {
            //resultText.text = "강화에 필요한 재료가 부족합니다.";
            ErrorMsg.Instance.ShowErrMsg("강화에 필요한 재료가 부족합니다.");
            return;
        }
        else//재료가 충분히 있음. 재료 아이탬 소모.
        {
            Require = EnchantInfo.ingredientRequire[x];

            for (int index = 0; index<PlayerData.Instance.invenSlot.Length; index++)
            {
                if(PlayerData.Instance.invenSlot[index]._itemCode == a._ingredient)
                {
                    if(PlayerData.Instance.invenSlot[index]._amount >= Require)
                    {
                        PlayerData.Instance.invenSlot[index]._amount -= Require;
                        Require = 0;
                        break;
                    }
                    else
                    {
                        Require -= PlayerData.Instance.invenSlot[index]._amount;
                        PlayerData.Instance.invenSlot[index]._amount = 0;
                    }
                }
            }

            UpdateInventory();
        }

        int i = Random.Range(0, 100);

        resultPanel.SetActive(true);
        EscInput.disable = true;
        equipIcon_result.sprite = equipIcon_checkPanel.sprite;

        if (i < EnchantInfo.enchantProbability[x]) // 강화 성공.
        {
            resultText.text = "<color=orange>강화 성공!</color>";
            successEffect.enabled = true;
            a._enchant += 1;
            a._abilityAmount[0] += 1;

            UpdateEquipPanel();
            UpdateEquipInfoPanel();
            UpdateCheckPanel();
        }
        else
        {
            resultText.text = "<color=red>강화 실패!</color>";
            successEffect.enabled = false;
        }
    }

    public void UpgradeBtnClick()
    {
        // 여기서 승급퀘 완료했는지 확인해야함.

        resultPanel.SetActive(true);
        resultText.text = "<color=orange>장비 승급!</color>";
        a._grade += 1;
        a._enchant = 0;
        a._abilityAmount[0] += 4;
        a._abilityAmount[1] += 2;

        UpdateEquipPanel();
        UpdateEquipInfoPanel();
        UpdateCheckPanel();
        UpdateArmorRankText();
    }

    public void HideResultPanel()
    {
        resultPanel.SetActive(false);
        EscInput.disable = false;
    }

    #endregion

    #region [[ 장비 ]]

    void UpdateArmorRankText()
    {
        PlayerData.Instance.ArmorRankCalc();
        armorRank.text = "종합 랭크 : " + Armor._gradeText[PlayerData.Instance.pc[PlayerData.Instance.selectedMainCharIndex].armorRank];
    }

    public void EquipBtnClick(int index)
    {
        equipInfoPanel.SetActive(true);
        EscInput.panel = equipInfoPanel;

        a = PlayerData.Instance.pc[selectedCardIndex].armor[index];

        equipIcon.sprite = equipBtn[index].transform.GetChild(0).GetComponent<Image>().sprite; //누른 버튼에 있는 아이콘을 가져옴.
        //만약 캐릭마다 장비 그림이 다르다면 캐릭 선택했을때 각 장비버튼 아이콘이 업데이트되야함.

        UpdateEquipInfoPanel();
    }

    public void UpdateEquipPanel()
    {
        for (int i = 0; i < 6; i++)
        {
            equipBtn[i].GetComponentInChildren<Text>().text =
                Armor._gradeText[PlayerData.Instance.pc[selectedCardIndex].armor[i]._grade] +
                " +<color=blue>" + PlayerData.Instance.pc[selectedCardIndex].armor[i]._enchant + "</color>";
        }
    }

    void UpdateEquipInfoPanel()
    {
        equipName.text = a._name;
        equipInfo.text = Armor._gradeText[a._grade] + " / +" + a._enchant;
        abilityA.text = Armor._abilityName[a._ability[0]] + " +" + a._abilityAmount[0];
        abilityB.text = Armor._abilityName[a._ability[1]] + " +" + a._abilityAmount[1];

        if (a._enchant >= 5)
        {
            enchantBtn.SetActive(false);
            if (a._grade < 4)
            {
                upgradeBtn.SetActive(true);
            }
            else
            {
                upgradeBtn.SetActive(false);
            }
        }
        else
        {
            enchantBtn.SetActive(true);
            upgradeBtn.SetActive(false);
        }
    }

    public void HideEquipInfoPanel()
    {
        equipInfoPanel.SetActive(false);
        EscInput.panel = MainMenu.Instance.heroPanel;
    }

    #endregion

    public void UpdateInventory()
    {
        for(int i = 0; i<PlayerData.Instance.invenSlot.Length; i++) // 수량이 0인 아이탬 인벤에서 제거.
        {
            if(PlayerData.Instance.invenSlot[i]._amount == 0)
            {
                PlayerData.Instance.invenSlot[i]._itemCode = -1;
            }
        }

        for (int i = 0; i<invenBtn.Length; i++)
        {
            Image icon = invenBtn[i].transform.GetChild(0).GetComponent<Image>(); // 아이탬 아이콘
            Text nameTxt = invenBtn[i].transform.GetChild(1).GetComponent<Text>(); // 아이탬 이름
            Text amountTxt = invenBtn[i].transform.GetChild(2).GetComponent<Text>(); // 아이탬 수량
            InvenSlot item = PlayerData.Instance.invenSlot[i];

            if (item._itemCode == -1) // 빈칸이면 지움.
            {
                nameTxt.text = "";
                amountTxt.text = "";
                icon.enabled = false;
                continue;
            }

            nameTxt.text = GameManager.itemInfoList[item._itemCode].name;
            if(GameManager.itemInfoList[item._itemCode].stack) // 스택불가 아이탬은 수량표시 안함.
            {
                amountTxt.text = item._amount.ToString();
            }
            else
            {
                amountTxt.text = "";
            }

            icon.enabled = true;
            icon.sprite = GameManager.itemInfoList[item._itemCode].icon;
        }
    }
}
