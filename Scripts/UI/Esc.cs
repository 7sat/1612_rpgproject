﻿using UnityEngine;
using System.Collections;

public class Esc : MonoBehaviour
{
    public GameObject targetPanel; // 돌아갈 패널

    public void Escape()
    {
        gameObject.SetActive(false);
        targetPanel.SetActive(true);

        EscInput.panel = targetPanel;
    }
}
