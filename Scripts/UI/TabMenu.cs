﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TabMenu : MonoBehaviour
{
    public GameObject[] subPanel;
    public Button[] tabBtn;

    void Start()
    {
        ShowTab(0);
    }

    public void ShowTab(int index)
    {
        HideAll();
        subPanel[0].SetActive(false);
        subPanel[index].SetActive(true);
        tabBtn[index].GetComponent<Image>().color = Color.gray;
    }

    void HideAll()
    {
        for(int i = 0; i<subPanel.Length; i++)
        {
            subPanel[i].SetActive(false);
            tabBtn[i].GetComponent<Image>().color = Color.white;
        }
    }
}
